import "regenerator-runtime/runtime";
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Big from "big.js";
import Form from "./components/Form";
import SignIn from "./components/SignIn";
import DoneLogo from "./done.svg";

//const BOATLOAD_OF_GAS = Big(3).times(10 ** 13).toFixed();
const DONATION = Big("0.000000000000000000000001")
  .times(10 ** 24)
  .toFixed();
const BOATLOAD_OF_GAS = Big(3)
  .times(10 ** 13)
  .toFixed();
const NEAR_FOR_CALL = Big("0.00125")
  .times(10 ** 24)
  .toFixed();
//const IMAGE_LINK = './DONE.svg'
//const DONATION = '10000000000000000000000';

//const IMAGE_LINK = 'https://bafybeicpwikgihx6dx36ivkjfsfoouylcxesficdlb4ghirh4vvtmqgzwu.ipfs.dweb.link';
const App = ({ contract, currentUser, nearConfig, wallet }) => {
  const [balance, setBalance] = useState(0);
  const [tokensToTake, setTokensToTake] = useState(100);
  const [tokensToTransfer, setTokensToTransfer] = useState(100);
  const [accountToTransfer, setAccountToTransfer] = useState("");

  const onChangeTokensToTake = (e) => setTokensToTake(e.target.value);
  const onChangeTokensToTransfer = (e) => {
    let transferTokens = e.target.value;
    if (transferTokens > balance) { transferTokens = balance} 
    setTokensToTransfer(transferTokens)
  };
  const onChangeAccountToTransfer = (e) => setAccountToTransfer(e.target.value);

  const getTokens = () => {
    // const { fieldset } = e.target.elements;

    // fieldset.disabled = true;
    // TODO: optimistically update page with new message,
    // update blockchain data in background
    // add uuid to each message, so we know which one is already known
    console.log("Go go go");
    console.log(currentUser.accountId, tokensToTake);
    contract
      .ft_mint(
        {
          receiver_id: currentUser.accountId,
          amount: parseInt(tokensToTake).toString(),
        },
        BOATLOAD_OF_GAS,
        NEAR_FOR_CALL
      )
      .then((res) => {
        console.log("Got it", res);
        return contract.ft_balance_of({
          account_id: currentUser.accountId,
        });
      })
      .then((res) => {
        console.log("Got ballance", res);
        return setBalance(Number(res));
      })
      .catch((e) => {
        console.error(e);
      });
  };

  const getBalance = () => {
    console.log("Go go go");
    console.log(currentUser.accountId, tokensToTake);
    contract
      .ft_balance_of({
        account_id: currentUser.accountId,
      })
      .then((res) => {
        console.log("Got ballance", res);
        return setBalance(Number(res));
      })
      .catch((e) => {
        console.error(e);
      });
  };

  useEffect(() => {
    // Обновляем заголовок документа с помощью API браузера
    if (!currentUser) {
      return;
    }
    getBalance();
  });

  const sendTokens = () => {
    // const { fieldset } = e.target.elements;

    // fieldset.disabled = true;
    // TODO: optimistically update page with new message,
    // update blockchain data in background
    // add uuid to each message, so we know which one is already known
    console.log("Go go go");
    console.log(accountToTransfer, tokensToTransfer);
    contract
      .ft_transfer(
        {
          receiver_id: accountToTransfer,
          amount: parseInt(tokensToTransfer).toString(),
        },
        BOATLOAD_OF_GAS,
        DONATION
      )
      .then((res) => {
        console.log("Got it", res);
        return contract.ft_balance_of({
          account_id: currentUser.accountId,
        });
      })
      .then((res) => {
        console.log("Got ballance", res);
        return setBalance(Number(res));
      })
      .catch((e) => {
        console.error(e);
      });
  };

  const signIn = () => {
    wallet
      .requestSignIn(nearConfig.contractName, "DONE token")
      .then((res) =>
        contract.storage_deposit(
          {
            account_id: currentUser.accountId,
          },
          BOATLOAD_OF_GAS,
          Big("0.00125")
            .times(10 ** 24)
            .toFixed()
        )
      )
      .then((res) =>
        contract.ft_balance_of({
          account_id: currentUser.accountId,
        })
      )
      .then((res) => setBalance(number(res)))
      .catch((e) => console.log(e));
  };

  const signOut = () => {
    wallet.signOut();
    window.location.replace(window.location.origin + window.location.pathname);
  };

  return (
    <main>
      <header>
        <h1>DONE token minting</h1>
        <p>
          <img className="ft" src={DoneLogo} />
        </p>
        {currentUser ? (
          <button onClick={signOut}>Log out</button>
        ) : (
          <button onClick={signIn}>Log in</button>
        )}
      </header>

      {currentUser ? (
        <div>
          <p>
            Dear, {currentUser.accountId}. Click button to mint FT to your
            wallet !
          </p>
          <p>Balance: {balance} <button className="mint-button" onClick={getBalance}>
         Get balance
        </button></p>
          <p>
            <label htmlFor="tokensToTake">DONE Tokens to take:</label>
            <input
              autoComplete="off"
              max="1000"
              min="0"
              step="1"
              value={tokensToTake}
              onChange={onChangeTokensToTake}
              type="number"
            />
            <span title="DONE Tokens"></span>
          </p>

          <button className="mint-button" onClick={getTokens}>
            Take DONE token to getting things DONE! (with NEARⓃ)!
          </button>

          <p>
            <label htmlFor="tokensToTransfer">DONE Tokens to transfer:</label>
            <input
              autoComplete="off"
              id="tokensToTransfer"
              max={balance}
              min="0"
              step="1"
              value={tokensToTransfer}
              onChange={onChangeTokensToTransfer}
              type="number"
            />
            <span title="DONE Tokens"></span>
          </p>
          <p>
            <label htmlFor="accountToTransfer">Account to transfer:</label>
            <input
              autoComplete="off"
              id="accountToTransfer"
              value={accountToTransfer}
              onChange={onChangeAccountToTransfer}
              type="string"
            />
          </p>

          <button className="mint-button" onClick={sendTokens}>
            Send DONE tokens to account! (with NEARⓃ)!
          </button>
        </div>
      ) : (
        <SignIn />
      )}
    </main>
  );
};

App.propTypes = {
  contract: PropTypes.shape({
    ft_mint: PropTypes.func.isRequired,
  }).isRequired,
  currentUser: PropTypes.shape({
    accountId: PropTypes.string.isRequired,
    balance: PropTypes.string.isRequired,
  }),
  nearConfig: PropTypes.shape({
    contractName: PropTypes.string.isRequired,
  }).isRequired,
  wallet: PropTypes.shape({
    requestSignIn: PropTypes.func.isRequired,
    signOut: PropTypes.func.isRequired,
  }).isRequired,
};

export default App;
